//pounds to kg
#include <stdio.h>
  float multiple = 0.45359;
  int main() {
        float kg, pound;

        printf("Enter weight in pound:");
        scanf("%f", &pound);

        kg = pound * multiple;

        printf("%.2f pound = %.2f KiloGram\n", pound, kg);
        return 0;
  }